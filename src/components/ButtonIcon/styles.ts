import { TouchableOpacity } from 'react-native'
import styled from 'styled-components/native'
import { MaterialIcons } from '@expo/vector-icons'

export type ButtonIconTypeStylesProps = 'PRIMARY' | 'SECONDARY'

type ButtonIconProps = {
  type: ButtonIconTypeStylesProps
}

export const Container = styled(TouchableOpacity)`
  width: 56px;
  height: 56px;
  margin-left: 12px;
  justify-content: center;
  align-items: center;
`

export const Icon = styled(MaterialIcons).attrs<ButtonIconProps>(
  ({ theme, type }) => ({
    size: 24,
    color: type === 'PRIMARY' ? theme.COLORS.GREEN_700 : theme.COLORS.RED,
  }),
)``
